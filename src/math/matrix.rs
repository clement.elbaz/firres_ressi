/**
    This file is part of Firres.

    Copyright (C) 2019  Inria

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use std::clone::Clone;
use std::fmt::Debug;
use std::fmt::Display;
use std::marker::Copy;
use std::ops::Add;
use std::ops::Index;
use std::ops::IndexMut;
use std::ops::Mul;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Matrix<T: Clone + Copy + Mul<Output = T> + Add<Output = T> + Default + Display + Debug> {
    values: Vec<Vec<T>>,
}

impl<T: Clone + Copy + Mul<Output = T> + Add<Output = T> + Default + Display + Debug> Matrix<T> {
    pub fn new(nb_columns: usize, nb_rows: usize, default_value: T) -> Matrix<T> {
        Matrix {
            values: vec![vec![default_value; nb_columns as usize]; nb_rows as usize],
        }
    }

    pub fn size(&self) -> (usize, usize) {
        (self.values[0].len(), self.values.len())
    }
}

impl<T: Clone + Copy + Mul<Output = T> + Add<Output = T> + Default + Display + Debug>
    Index<(usize, usize)> for Matrix<T>
{
    type Output = T;

    fn index(&self, value: (usize, usize)) -> &T {
        &self.values[value.0 as usize][value.1 as usize]
    }
}

impl<T: Clone + Copy + Mul<Output = T> + Add<Output = T> + Default + Display + Debug>
    IndexMut<(usize, usize)> for Matrix<T>
{
    fn index_mut<'a>(&'a mut self, value: (usize, usize)) -> &'a mut T {
        &mut self.values[value.0 as usize][value.1 as usize]
    }
}

impl<T: Clone + Copy + Mul<Output = T> + Add<Output = T> + Default + Display + Debug> Mul<Matrix<T>>
    for Matrix<T>
{
    type Output = Matrix<T>;

    fn mul(self, rhs: Matrix<T>) -> Matrix<T> {
        // We need number of columns in first matrix to be equals to number of row in the second matrix
        assert_eq!(self.size().0, rhs.size().1);

        let nb_terms = self.size().0; // == rhs.size().1 (see assert)
        let nb_rows = self.size().1;
        let nb_columns = rhs.size().0;

        let mut result = Matrix::new(nb_columns, nb_rows, Default::default());

        for i in 0..nb_rows {
            for j in 0..nb_columns {
                let mut sum_of_terms = Default::default();

                for k in 0..nb_terms {
                    sum_of_terms = sum_of_terms + self[(i, k)] * rhs[(k, j)];
                }

                result[(i, j)] = sum_of_terms;
            }
        }

        result
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::panic;

    #[test]
    fn test_init() {
        let mut matrix1 = Matrix::new(2, 2, 1);

        assert_eq!((2, 2), matrix1.size());

        assert_eq!(1, matrix1[(0, 0)]);
        assert_eq!(1, matrix1[(0, 1)]);
        assert_eq!(1, matrix1[(1, 0)]);
        assert_eq!(1, matrix1[(1, 1)]);

        let result = panic::catch_unwind(|| matrix1[(1, 2)]);
        assert!(result.is_err());

        let result2 = panic::catch_unwind(|| matrix1[(2, 1)]);
        assert!(result2.is_err());

        matrix1[(0, 1)] = 5;
        assert_eq!(1, matrix1[(0, 0)]);
        assert_eq!(5, matrix1[(0, 1)]);
        assert_eq!(1, matrix1[(1, 0)]);
        assert_eq!(1, matrix1[(1, 1)]);
    }

    #[test]
    fn test_mul() {
        let mut matrix1 = Matrix::new(2, 2, 1);

        matrix1[(0, 0)] = 1;
        matrix1[(0, 1)] = 2;
        matrix1[(1, 0)] = 3;
        matrix1[(1, 1)] = 4;

        println!("{:?}", &matrix1);

        let mut matrix2 = Matrix::new(3, 2, 1);

        matrix2[(0, 0)] = 5;
        matrix2[(0, 1)] = 6;
        matrix2[(0, 2)] = 7;
        matrix2[(1, 0)] = 8;
        matrix2[(1, 1)] = 9;
        matrix2[(1, 2)] = 10;

        println!("{:?}", &matrix2);

        let result = matrix1 * matrix2;

        assert_eq!((3, 2), result.size());

        assert_eq!(21, result[(0, 0)]);
        assert_eq!(24, result[(0, 1)]);
        assert_eq!(27, result[(0, 2)]);
        assert_eq!(47, result[(1, 0)]);
        assert_eq!(54, result[(1, 1)]);
        assert_eq!(61, result[(1, 2)]);
    }

}
