/**
    This file is part of Firres.

    Copyright (C) 2019  Inria

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use cleanutils;
use std::collections::BTreeMap;

#[derive(Debug)]
pub struct WordIndex {
    index: BTreeMap<String, WordInIndexEntry>,
    document_total: u64,
}

impl WordIndex {
    pub fn new() -> WordIndex {
        WordIndex {
            index: BTreeMap::new(),
            document_total: 0,
        }
    }

    pub fn publish_document(&mut self, document: &String) -> Vec<WordInDocEntry> {
        let mut index_document: BTreeMap<String, WordInDocEntry> = BTreeMap::new();

        let cleaned_document = cleanutils::clean_document(document);

        for uncleaned_word in cleaned_document.split_whitespace() {
            let word = cleanutils::clean_word(uncleaned_word.to_string());

            // This is not the best thing performance-wise (2 or 3 traversals are needed on the B-Tree every time while we could theoretically go with one), but it is the most readable thing I've found within Rust memory model.
            if !index_document.contains_key(&word) {
                index_document.insert(
                    word.clone(),
                    WordInDocEntry {
                        word: word.clone(),
                        term_frequency: 0,
                    },
                );
            }

            let word_doc_entry = index_document.get_mut(&word).unwrap(); // We just made sure there was a value in any case

            word_doc_entry.term_frequency += 1;
        }

        let mut indexed_document: Vec<WordInDocEntry> = index_document.values().cloned().collect();

        indexed_document.sort_by_key(|k| k.word.clone());

        self.publish_indexed_document(&indexed_document);

        indexed_document
    }

    fn publish_indexed_document(&mut self, indexed_document: &Vec<WordInDocEntry>) {
        self.document_total += 1;

        for word_doc_entry in indexed_document {
            let word = &word_doc_entry.word;

            if !self.index.contains_key(word) {
                self.index.insert(
                    word.clone(),
                    WordInIndexEntry {
                        word: word.clone(),
                        document_with_word_count: 0,
                    },
                );
            }

            let word_index_entry = self.index.get_mut(word).unwrap(); // We just made sure there was a value in any case

            word_index_entry.document_with_word_count += 1
        }
    }

    fn get_idf(&self, word: &String) -> f64 {
        let document_total = self.document_total as f64;
        let documents_with_word = match self.index.get(word) {
            Some(word_entry) => word_entry.document_with_word_count,
            None => 0,
        } as f64;

        // We use the '+1' denominator trick to avoid division by zero, see https://en.wikipedia.org/wiki/Tf%E2%80%93idf#Inverse_document_frequency_2
        (document_total / (documents_with_word + 1.0)).ln()
    }

    pub fn get_tf_idf(&self, word_entry: &WordInDocEntry) -> f64 {
        let tf = word_entry.term_frequency as f64;
        let idf = self.get_idf(&word_entry.word);

        tf * idf
    }
}

#[derive(Debug)]
pub struct WordInIndexEntry {
    pub word: String,
    pub document_with_word_count: u64,
}

#[derive(Clone, PartialEq, Debug)]
pub struct WordInDocEntry {
    pub word: String,
    pub term_frequency: u8,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_index() {
        let mut index = WordIndex::new();

        let entry1 = index.publish_document(&"The Lord of the Rings".to_string());
        let entry2 = index.publish_document(&"The Lord of the Flies".to_string());
        let entry3 = index.publish_document(&"The Ace of Spades".to_string());

        assert_eq!(
            vec![
                WordInDocEntry {
                    word: "lord".to_string(),
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "of".to_string(),
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "rings".to_string(),
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the".to_string(),
                    term_frequency: 2,
                },
            ],
            entry1
        );

        assert_eq!(
            vec![
                WordInDocEntry {
                    word: "flies".to_string(),
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "lord".to_string(),
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "of".to_string(),
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the".to_string(),
                    term_frequency: 2,
                },
            ],
            entry2
        );

        assert_eq!(
            vec![
                WordInDocEntry {
                    word: "ace".to_string(),
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "of".to_string(),
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "spades".to_string(),
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the".to_string(),
                    term_frequency: 1,
                },
            ],
            entry3
        );

        // We use the '+1' denominator trick to avoid division by zero, see https://en.wikipedia.org/wiki/Tf%E2%80%93idf#Inverse_document_frequency_2
        assert_eq!((3.0 / 2.0 as f64).ln(), index.get_idf(&"ace".to_string()));
        assert_eq!((3.0 / 2.0 as f64).ln(), index.get_idf(&"flies".to_string()));
        assert_eq!((3.0 / 3.0 as f64).ln(), index.get_idf(&"lord".to_string()));
        assert_eq!((3.0 / 4.0 as f64).ln(), index.get_idf(&"of".to_string()));
        assert_eq!((3.0 / 2.0 as f64).ln(), index.get_idf(&"rings".to_string()));
        assert_eq!(
            (3.0 / 2.0 as f64).ln(),
            index.get_idf(&"spades".to_string())
        );
        assert_eq!((3.0 / 4.0 as f64).ln(), index.get_idf(&"the".to_string()));

        assert_eq!(2.0 * (3.0 / 4.0 as f64).ln(), index.get_tf_idf(&entry1[3]));
        assert_eq!(1.0 * (3.0 / 2.0 as f64).ln(), index.get_tf_idf(&entry2[0]));
    }

}
